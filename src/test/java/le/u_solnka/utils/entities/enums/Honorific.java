package le.u_solnka.utils.entities.enums;

/**
 * Represents possible user honorifics.
 */
public enum Honorific
{
    SUDAR,
    SUDARYNJA,
    BARY6NJA
}